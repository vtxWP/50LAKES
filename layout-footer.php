						<?php 
                        $sidebarposition = esc_attr(50lakes_get_option( '50lakes_sidebar_position' ,'two-col-left')); 
                		
						
                        if(is_home()){
							$pid = get_option('page_for_posts');
						}else{
							$pid = '';
						}
						$custom_fields = 50lakes_get_customdata($pid);
                        
                        $pagelayout = $sidebarposition;
                        
						if(50lakes_get_metabox('50lakes_layout') && 50lakes_get_metabox('50lakes_layout')!='default' && is_search()!='default'){
							$pagelayout = esc_attr(50lakes_get_metabox('50lakes_layout'));
						}
						?>
						
                        		<div class="clear"></div>
                            </div><!-- main -->
                            
                            <?php
							if(is_active_sidebar('contentbottom') ){ 
							?>
							<div class="row">
								<div class="twelve columns">
                                    <div class="custom-position contentbottom-container">
                                        <?php if ( ! dynamic_sidebar( 'contentbottom' ) ){ }?>
                                        <div class="clear"></div>
                                    </div>
                                </div>
							</div>
							<?php 
							}
							?>
                            
                            <div class="clear"></div>
                        </section><!-- content -->
                        
                        <?php if($pagelayout!='one-col'){ ?>
                        
                        <aside id="sidebar" class="sidebarcol columns <?php if($pagelayout=="two-col-left"){echo "positionright";}else{echo "positionleft";}?>">
                            <?php get_sidebar();?>  
                        </aside><!-- sidebar -->
                        
                        <?php } ?>
                        <div class="clear"></div>
                        </section><!-- END #maincontent -->
                        
                        <div class="clear"></div>
                    </div>
                </div><!-- END container -->
                </div><!-- END maincontent-container --> 
                   
				<?php if(is_active_sidebar('mainbottom') ){ ?>
               
                    <div class="custom-position mainbottom-container">
                        <?php if ( ! dynamic_sidebar( 'mainbottom' ) ){ } ?>
                        <div class="clear"></div>
                    </div>
            
                <?php } ?>
                        

            </div><!-- END maincontainer -->
        </div><!-- END outermain -->
        <!-- END MAIN CONTENT -->
        
