<?php
/**
 * Loads up all the widgets defined by this theme. Note that this function will not work for versions of WordPress 2.7 or lower
 *
 */


$path_featureswidget = get_template_directory() . '/includes/widgets/50lakes-features-widget.php';
if(file_exists($path_featureswidget)) include_once ($path_featureswidget);

$path_latestnwwidget = get_template_directory() . '/includes/widgets/50lakes-latestnews-widget.php';
if(file_exists($path_latestnwwidget)) include_once ($path_latestnwwidget);

$path_testiwidget = get_template_directory() . '/includes/widgets/50lakes-testimonial-widget.php';
if(file_exists($path_testiwidget)) include_once ($path_testiwidget);

$path_teamwidget = get_template_directory() . '/includes/widgets/50lakes-team-widget.php';
if(file_exists($path_teamwidget)) include_once ($path_teamwidget);

$path_actionwidget = get_template_directory() . '/includes/widgets/50lakes-action-widget.php';
if(file_exists($path_actionwidget)) include_once ($path_actionwidget);

$path_imgcarouselwidget = get_template_directory() . '/includes/widgets/50lakes-imagecarousel-widget.php';
if(file_exists($path_imgcarouselwidget)) include_once ($path_imgcarouselwidget);

$path_magazinewidget = get_template_directory() . '/includes/widgets/50lakes-magazine-widget.php';
if(file_exists($path_magazinewidget)) include_once ($path_magazinewidget);

if( function_exists('is_woocommerce')){
	$path_woofpwidget = get_template_directory() . '/includes/widgets/50lakes-product-widget.php';
	if(file_exists($path_woofpwidget)) include_once ($path_woofpwidget);
}

/* new */
$path_portfoliowidget = get_template_directory() . '/includes/widgets/50lakes-portfolio-widget.php';
if(file_exists($path_portfoliowidget)) include_once ($path_portfoliowidget);



add_action("widgets_init", "50lakes_theme_widgets");

function 50lakes_theme_widgets() {
	register_widget("50lakes_PortfolioWidget");
	register_widget("50lakes_FeaturesWidget");
	register_widget("50lakes_TestimonialWidget");
	register_widget("50lakes_TeamWidget");
	register_widget("50lakes_PCarouselWidget");
	register_widget("50lakes_CallToActionWidget");
	register_widget("50lakes_LatestNewsWidget");
	register_widget("50lakes_MagazineWidget");
	
	
	if( function_exists('is_woocommerce')){
		register_widget("50lakes_WooProductWidget");
	}
}