<?php

//if no plugin Theme Options
function 50lakes_get_option( $name, $default = false ) {
	return 50lakes_of_get_option($name, $default);
}


/* 
 * Helper function to return the theme option value. If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 * This code allows the theme to work without errors if the Options Framework plugin has been disabled.
 */
if ( !function_exists( '50lakes_of_get_option' ) ) {
	function 50lakes_of_get_option($name, $default = false) {
		
		$optionsframework_settings = get_option('optionsframework');
		
		// Gets the unique option id
		$option_name = $optionsframework_settings['id'];
		
		if ( get_option($option_name) ) {
			$options = get_option($option_name);
		}
			
		if ( !empty($options[$name]) ) {
			return $options[$name];
		} else {
			return $default;
		}
	}
}


/* Metabox */
function 50lakes_get_metabox( $field_id, $args = array(), $post_id = null ) {
	return 50lakes_get_rwmb_meta( $field_id, $args , $post_id );
}


if ( !function_exists( '50lakes_get_rwmb_meta' ) ) {
	function 50lakes_get_rwmb_meta( $field_id, $args = array(), $post_id = null) {
			$custom_fields = 50lakes_get_customdata();
			if(function_exists('rwmb_meta')){
				if(rwmb_meta( $field_id, $args, $post_id)){
					return rwmb_meta( $field_id, $args, $post_id);
				}elseif(isset( $custom_fields[$field_id] )){
					//return $custom_fields['50lakes_layout'];
				}
				
			}elseif(isset( $custom_fields[$field_id] )){
				return $custom_fields[$field_id];
			}else{
				return false;
			}
			
	}
}