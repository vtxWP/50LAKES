<?php
// get website title
if(!function_exists("50lakes_footer_text")){
	function 50lakes_footer_text(){
	
		$foot= wp_kses_post(50lakes_get_option( '50lakes_footer'));
		if($foot!=""){
        	echo $foot;
        }
		
	}// end 50lakes_footer_text()
}

// Copyright
if(!function_exists("50lakes_copyright_text")){
	function 50lakes_copyright_text(){

			_e('Copyright', '50lakes'); echo ' &copy; ';
			
				echo date('Y') . ' <a href="'.esc_url( home_url( '/' ) ).'">'.get_bloginfo('name') .'</a>.';
			?>
			<?php _e(' Designed by', '50lakes'); ?>	<a href="<?php echo esc_url( __( 'http://www.50lakesthemes.com', '50lakes' ) ); ?>" title=""><?php _e('50lakes Themes','50lakes')?></a>.
            
        <?php 
		
	}// end 50lakes_copyright_text()
}

if(!function_exists("50lakes_foot")){
	function 50lakes_foot(){
		do_action("50lakes_foot");
	}
}
add_action("wp_footer","50lakes_foot",20);

?>
