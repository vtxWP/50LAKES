<?php
function 50lakes_sidebar_init() {

	register_sidebar( array(
		'name' 					=> __( 'Primary Sidebar', '50lakes' ),
		'id' 						=> 'primary-sidebar',
		'description' 		=> __( 'Located at the left/right side of archives, single and search.', '50lakes' ),
		'before_widget' 	=> '<ul><li id="%1$s" class="widget-container %2$s"><div class="box">',
		'after_widget' 		=> '<div class="clear"></div></div></li></ul>',
		'before_title' 		=> '<h3 class="widget-title">',
		'after_title' 			=> '</h3>',
	));	
	
}



function 50lakes_maintop_sidebar(){	
		register_sidebar(array(
		'name'          => __('Main Top', '50lakes' ),
		'id'         	=> 'maintop',
		'description'   => __( 'Located at the top of the content top.', '50lakes' ),
		'before_widget' => '<div class="widget-maintop"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
}

function 50lakes_contenttop_sidebar(){
	
	register_sidebar(array(
		'name'          => __('Content Top', '50lakes' ),
		'id'         	=> 'contenttop',
		'description'   => __( 'Located at the top of the content.', '50lakes' ),
		'before_widget' => '<div class="widget-contenttop"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
}

function 50lakes_contentbottom_sidebar(){
	register_sidebar(array(
		'name'          => __('Content Bottom', '50lakes' ),
		'id'         	=> 'contentbottom',
		'description'   => __( 'Located at the bottom of the content.', '50lakes' ),
		'before_widget' => '<div class="widget-contentbottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
}


function 50lakes_mainbottom_sidebar(){	
	register_sidebar(array(
		'name'          => __('Main Bottom', '50lakes' ),
		'id'         	=> 'mainbottom',
		'description'   => __( 'Located at the bottom of the content bottom.', '50lakes' ),
		'before_widget' => '<div class="widget-mainbottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));

}


function 50lakes_footer1_sidebar_init(){

	register_sidebar(array(
		'name'          => __('Footer 1', '50lakes' ),
		'id'         	=> 'footer1',
		'description'   => __( 'Located at the footer column 1.', '50lakes' ),
		'before_widget' => '<div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
	
}

function 50lakes_footer2_sidebar_init(){

	register_sidebar(array(
		'name'          => __('Footer 2', '50lakes' ),
		'id'         	=> 'footer2',
		'description'   => __( 'Located at the footer column 2.', '50lakes' ),
		'before_widget' => '<div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));	
	
}

function 50lakes_footer3_sidebar_init(){

	register_sidebar(array(
		'name'          => __('Footer 3', '50lakes' ),
		'id'         	=> 'footer3',
		'description'   => __( 'Located at the footer column 3.', '50lakes' ),
		'before_widget' => '<div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
	
}

function 50lakes_footer4_sidebar_init(){

	register_sidebar(array(
		'name'          => __('Footer 4', '50lakes' ),
		'id'         	=> 'footer4',
		'description'   => __( 'Located at the footer column 4.', '50lakes' ),
		'before_widget' => '<div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
	
}

function 50lakes_footer5_sidebar_init(){

	register_sidebar(array(
		'name'          => __('Footer 5', '50lakes' ),
		'id'         	=> 'footer5',
		'description'   => __( 'Located at the footer column 5.', '50lakes' ),
		'before_widget' => '<div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
	
}

function 50lakes_footer6_sidebar_init(){

	register_sidebar(array(
		'name'          => __('Footer 6', '50lakes' ),
		'id'         	=> 'footer6',
		'description'   => __( 'Located at the footer column 6.', '50lakes' ),
		'before_widget' => '<div class="widget-bottom"><ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '<div class="clear"></div></li></ul><div class="clear"></div></div>',
		'before_title' 	=> '<h3 class="widget-title">',
		'after_title' 	=> '</h3>',
	));
	
}



/** Register sidebars by running 50lakes_sidebar_init() on the widgets_init hook. */
add_action( 'widgets_init', '50lakes_sidebar_init' );

add_action( 'widgets_init', '50lakes_maintop_sidebar' );
add_action( 'widgets_init', '50lakes_contenttop_sidebar' );

add_action( 'widgets_init', '50lakes_contentbottom_sidebar' );
add_action( 'widgets_init', '50lakes_mainbottom_sidebar' );

