<?php
function 50lakes_child_setup() {
	
	add_action( 'widgets_init', '50lakes_footer1_sidebar_init' );
	add_action( 'widgets_init', '50lakes_footer2_sidebar_init' );
	add_action( 'widgets_init', '50lakes_footer3_sidebar_init' );
	add_action( 'widgets_init', '50lakes_footer4_sidebar_init' );

}
add_action( 'after_setup_theme', '50lakes_child_setup' );


/* class 50lakes */
add_filter( 'body_class', '50lakes_body_class' );
function 50lakes_body_class( $classes ) {
	// add 'class-name' to the $classes array
	$classes[] = 'childclass';
	// return the $classes array
	return $classes;
}
