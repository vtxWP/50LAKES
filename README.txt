== 50LAKES ==

Author: hyperclock
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Version: 1.0
Tested up to: WordPress 8.1
Tags: one-column, two-columns, left-sidebar, right-sidebar, responsive-layout, custom-menu, post-formats, theme-options, translation-ready
Text Domain: 50lakes

== Description ==

50LAKES is a theme for WordPress. Perfect to use as business, portfolio, gallery, blog, personal and any kind of websites. Theme features 9 widget area, 8 custom widget and simple Theme Option for full CMS control with easy Logo Upload, etc. 50LAKES is Multilingual Ready (WPML), WooCommerce compatible, Retina Ready, RTL-Language Support, Translation Ready, Responsive, Flexible, Cross Browser Compatible, Search Engine Optimized (SEO), Font Awesome ready and standard compliant with HTML5 and CSS3.


== LICENSES ==

Screenshot URL: /screenshot.png/
50LAKES WordPress Theme, Copyright 2017 by hyperclock
50LAKES is distributed under the terms of the GNU GPL

50LAKES is based on 50lakes Framework Base/Starter Theme, Copyright 2015-2016 50lakesthemes  http://www.50lakesthemes.com/
50lakes Framework is distributed under the terms of the GNU GPL: https://www.gnu.org/licenses/gpl-3.0.en.html


== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's ZIP file. Click Install Now.
3. Click Activate to use your new theme right away.

== The plugins use in this theme ==

Options Famework for Theme Options
Plugin URI: https://wordpress.org/plugins/options-framework/
License: GPLv2 (https://github.com/devinsays/options-framework-plugin)

Meta Box for the layout you want on this specific post/page. Overrides default site layout.
Plugin URI: https://wordpress.org/plugins/meta-box/
License: GNU General Public License version 2+ (https://metabox.io/faq/)


== Where can I find the theme options? ==

In your admin panel, go to Appearance -> Theme Options


== Credit ==

* FlexSlider by WooThemes under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
* PrettyPhoto (https://github.com/scaron/prettyphoto) Licensed under GPLv2 or Creative Commons 2.5 license.
* Tinynav (https://github.com/viljamis/TinyNav.js), Copyright (c) 2011-2014 Viljami Salminen, http://viljamis.com/, Licensed under the MIT license.
* Retina.js (https://github.com/imulus/retinajs/blob/master/LICENSE) Licensed under the MIT license
* Superfish (https://github.com/joeldbirch/superfish) Dual licensed under the MIT and GPL licenses: http://www.opensource.org/licenses/mit-license.php, http://www.gnu.org/licenses/gpl.html
* Supersubs - licensed under the MIT and GPL licenses
* Modernizr (https://github.com/Modernizr/Modernizr) licensed under MIT licenses
* HTML5 - Licensed under GPLv2 and MIT licenses.
* FontAwesome by Dave Gandy Font License (http://fontawesome.io) licensed under the SIL OFL 1.1 (http://scripts.sil.org/OFL)
* FontAwesome by Dave Gandy Code License (http://fontawesome.io) licensed under the MIT License (http://opensource.org/licenses/mit-license.html)
* TGM Plugin Activation library (https://github.com/thomasgriffin/TGM-Plugin-Activation) licensed under GNU General Public License v2.0 or later


== IMAGE CREDITS ==
Stock Images - https://www.pixelbay.com/
Licensed under the terms of CC0
Header image 1 - https://#/
Header image 2 - https://#/
Header image 3 - https://#/



== Change log ==
- Updated:
- Fixed:
- Added:
