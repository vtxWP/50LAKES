<?php
/**
 * The Sidebar containing the post widget areas.
 *
 * @package WordPress
 * @subpackage 50lakes
 * @since 50lakes 1.0
 */

?>
<div class="widget-area">
	<?php if ( ! dynamic_sidebar( "primary-sidebar" ) ) : ?><?php endif; // end general widget area ?>
    <div class="clear"></div>
</div>